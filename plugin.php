<?php
/*
Plugin Name: Top Categories And Tags
Plugin URI: https://planetjon.ca/projects/top-cat-tags/
Description: A breakdown of your most common blog classifications
Version: 1.0
Requires at least: 3.5.0
Tested up to: 5.4
Requires PHP: 5.4
Author: Jonathan Weatherhead
Author URI: https://planetjon.ca
Text Domain: top-cats-tags
Domain Path: /languages/
License: GPL2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace planetjon\wordpress\topcatstags;

use WP_Widget, WP_Term;

const topcatstagskey = 'topcatstags';

function topCatsTags() {
	$data = getTopCatsTags();

	include 'template.php';
}

function getTopCatsTags() {
	if( $topCatsTagsData = apply_filters( topcatstagskey . '_prefetch_data', null ) ) {
		return $topCatsTagsData;
	}

	// extract only what is needed
	$taxMapper = function( WP_Term $tax ) {
		return (object) [
			'term_id' => $tax->term_id,
			'name' => $tax->name,
			'count' => $tax->count
		];
	};

	// get total published posts
	// fast approximation. Could sum counts of all categories for more accuracy
	$postCount = wp_count_posts()->publish;
	
	// get top 3 used categories
	$topCats = array_map( $taxMapper,
		get_terms( [
			'taxonomy' => 'category',
			'orderby' => 'count',
			'order' => 'DESC',
			'number' => '3',
			'childless' => true,
			'update_term_meta_cache' => false
		] )
	);
		
	$topCatsTagsMap = [];
	foreach( $topCats as $topCat ) {
		// get posts in top category
		$catPosts = get_posts( [
			'category__in' => [$topCat->term_id],
			'fields' => 'ids',
			'numberposts' => -1,
			'cache_results' => false,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );

		// get most used tags in posts in most used categories
		// slicing because number option is buggy in conjunction with object_ids
		$topTags = array_map( $taxMapper,
			array_slice( get_terms( [
				'taxonomy' => 'post_tag',
				'orderby' => 'count',
				'order' => 'DESC',
				'object_ids' => $catPosts,
				'update_term_meta_cache' => false
			] ), 0, 3 )
		);

		$topCatsTagsMap[$topCat->term_id] = $topTags;
	}

	return apply_filters( topcatstagskey . '_data', (object) compact( 'postCount', 'topCats', 'topCatsTagsMap' ) );
}

function _prefetchData() {
	if( $topCatsTagsData = get_transient( topcatstagskey . '_data' ) ) {
		return json_decode( $topCatsTagsData );
	}
}

function _preserveData( $data ) {
	set_transient( topcatstagskey . '_data', json_encode( $data ), DAY_IN_SECONDS );

	return $data;
}

class TopCatsTagsWidget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'topcatstags_widget',
			__( 'Top Categories And Tags', 'top-cats-tags' ),
			[ 'description' => __( 'Feature top categories and tags.', 'top-cats-tags' ) ]
		);
	}

	public function widget( $widget, $fields ) {
		topCatsTags();
	}
}

add_action( 'widgets_init', function() {
	register_widget( __NAMESPACE__ . '\TopCatsTagsWidget' );
} );

add_filter( topcatstagskey . '_prefetch_data', __NAMESPACE__ . '\_prefetchData' );
add_filter( topcatstagskey . '_data', __NAMESPACE__ . '\_preserveData' );
