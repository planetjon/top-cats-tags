<div class="top-cats-tags-container container">
<?php foreach( $data->topCats as $topCat ) : ?>
    <div class="top-category">
        <span class="category">
            <span class="usage" data-usage="<?php printf( '%d', 100 * $topCat->count / $data->postCount ) ?>">
                <span class="block" style="width:<?php printf( '%d%%', 100 * $topCat->count / $data->postCount ) ?>"></span>
            </span>
            <a href="<?php echo get_term_link( (int) $topCat->term_id, 'category' ) ?>"><?php echo esc_html( $topCat->name ) ?></a>
        </span>
        <?php foreach( $data->topCatsTagsMap->{$topCat->term_id} as $topTag ) : ?>
            <div class="top-tag">
                <span class="usage" data-usage="<?php printf( '%d', 100 * $topTag->count / $data->postCount ) ?>">
                    <span class="block" style="width:<?php printf( '%d%%', 100 * $topTag->count / $data->postCount ) ?>"></span>
                </span>
                <span class="tag">
                    <a href="<?php echo get_term_link( (int) $topTag->term_id, 'post_tag' ) ?>"><?php echo esc_html( $topTag->name ) ?></a>
                </span>
            </div>
        <?php endforeach ?>
    </div>
<?php endforeach ?>
</div>
